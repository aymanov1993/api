<?php

use Phalcon\Acl;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl\Role;
use Phalcon\Acl\Adapter\Memory as AclList;
use Phalcon\Acl\Resource;

class SecurityPlugin extends Plugin
{
    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        // Check whether the "auth" variable exists in session to define the active role
        $token = $this->request->get("token");
        $user_id = $this->redis->hget($token, "user_id");
        $role = $this->redis->hget($token, "user_role");
        
        if (empty($role)) {
            $role = "guest";
        }

        // Take the active controller/action from the dispatcher
        $controller = $dispatcher->getControllerName();
        $action     = $dispatcher->getActionName();

        // Obtain the ACL list
        $acl = $this->getAcl();

        // Check if the Role have access to the controller (resource)
        $allowed = $acl->isAllowed($role, $controller, $action);

        if (!$allowed) {
            $error_message = "Unauthorized access";
            $error_id = Generic::errorLog($error_message, $this->urls['requestUrl']);

            Generic::returnJsonResponse(401, "Unauthorized",
                [
                    "errors" => array_values(
                        [
                            [
                                "errorLink" => $this->urls['errorsUrl']. "/{$error_id}",
                                "errorMessage" => $error_message
                            ]
                        ]
                    )
                ]
            );
            
            // Returning "false" we tell to the dispatcher to stop the current operation
            return false;
        }
    }
    
    private function getAcl(){
        // Create the ACL
        $acl = new AclList();
        $config = $this->config;

        // The default action is DENY access
        $acl->setDefaultAction(
            Acl::DENY
        );

        // Register two roles, Users is registered users
        // and guests are users without a defined identity
        $roles = [
            "admin"  => new Role("admin"),
            "guest" => new Role("guest"),
            "user" => new Role("user"),
        ];

        foreach ($roles as $role) {
            $acl->addRole($role);
        }
        
        // admin area resources (backend)
        $adminResources = $config->adminResources->toArray();

        foreach ($adminResources as $resourceName => $actions) {
            $acl->addResource(
                new Resource($resourceName),
                $actions
            );
        }

        // user area resources (backend)
        $userResources = $config->userResources->toArray();

        foreach ($userResources as $resourceName => $actions) {
            $acl->addResource(
                new Resource($resourceName),
                $actions
            );
        }
        
        // Public area resources (frontend)
        $guestResources = $config->guestResources->toArray();

        foreach ($guestResources as $resourceName => $actions) {
            $acl->addResource(
                new Resource($resourceName),
                $actions
            );
        }
        
        // Grant access to public areas to both users and guests
        foreach ($roles as $role) {
            foreach ($userResources as $resource => $actions) {
                $acl->allow(
                    "user",
                    $resource,
                    $actions
                );
            }
        }

        // Grant access to private area only to role Users
        foreach ($adminResources as $resource => $actions) {
            foreach ($actions as $action) {
                $acl->allow(
                    "admin",
                    $resource,
                    $action
                );
            }
        }
        
        foreach ($guestResources as $resource => $actions) {
            foreach ($actions as $action) {
                $acl->allow(
                    "guest",
                    $resource,
                    $action
                );
            }
        }
        
        return $acl;
    }
}