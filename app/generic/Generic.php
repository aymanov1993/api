<?php

use Phalcon\Validation;
use Phalcon\Validation\Validator\Regex as RegexValidator;
use Phalcon\Di;
use Phalcon\Http\Response;

class Generic
{
    public static function validateForm(array $param, array $pattern, array $message, array $form)
    {
        $validator = new Validation();
        $validator->add(
            $param,
            new RegexValidator(
                [
                    "pattern" => $pattern,
                    "message" => $message
                ]
            )
        );
        
        $messages = $validator->validate($form);
        return $messages;
    }
    
    public static function errorsMessagesArr($messages) : array
    {
        $urls = Di::getDefault()->getUrls();
        $errors_arr = [];
        foreach ($messages as $message) {
            $error_message = $message->getMessage();
            $error_id = self::errorLog($error_message, $urls['requestUrl']);

            $errors_arr[] = ["errorLink" => $urls['errorsUrl'] . "/{$error_id}", "errorMessage" => $error_message];
        }
        
        return $errors_arr;
    }
    
    public static function errorLog(string $error_message, string $error_link) : int
    {
        $error = new Errors();
        $error->error_message = $error_message;
        $error->error_link = $error_link;
        $error->save();
        
        return $error->error_id;
    }
    
    public static function returnJsonResponse(int $code, string $message, array $contentArr)
    {
        $response = new Response();

        $response->setStatusCode($code, $message);
        $response->setJsonContent($contentArr);
        
        $response->send();
    }
    
    public static function returnXmlResponse(int $code, string $message, $contentArr)
    {
        $response = new Response();

        $response->setStatusCode($code, $message);
        
        $xml_user_info = new SimpleXMLElement("<?xml version=\"1.0\"?><posts_info></posts_info>");
        self::arrayToXml($contentArr, $xml_user_info);
        
        $response->setHeader("Content-Type", "application/xml");
        $response->setContent($xml_user_info->asXML());
        $response->send();
    }
    
    public static function arrayToXml($array, &$xml_user_info)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)){
                    $subnode = $xml_user_info->addChild("$key");
                    self::arrayToXml($value, $subnode);
                } else {
                    $subnode = $xml_user_info->addChild("post$key");
                    self::arrayToXml($value, $subnode);
                }
            } else {
                //var_dump($value);
                $xml_user_info->addChild("$key",htmlspecialchars($value));
            }
        }
    }
}