<?php
use Phalcon\Loader;

$loader = new Loader();

$loader->registerDirs(
        array(
            $config->application->controllersDir,
            $config->application->modelsDir,
            $config->application->pluginsDir,
            $config->application->redisDir,
            $config->application->genericDir
        )
    );
$loader->register();