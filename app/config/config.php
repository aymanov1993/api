<?php
use Phalcon\Config;

return new Config([
    'database' => [
        'port' => '5432',
        'host' => 'ec2-23-23-227-188.compute-1.amazonaws.com',
        'username' => 'tdiwjpehlgvuld',
        'password' => '3acbad8e0938fa24a095b3bf4faa87243fb9e66ae1fbdcebbbcf2beb131ce4cb',
        'dbname' => 'd14schr1n52nu1'
    ],
    'redis' => [
        'host' => 'angelfish.redistogo.com',
        'port' => '11631',
        'password' => 'c6e82dc52d3c359463c6798bcc52d4d0'
    ],
    'application' => [
        'controllersDir' => APP_PATH . '/controllers/',
        'modelsDir'      => APP_PATH . '/models/',
        'pluginsDir'     => APP_PATH . '/plugins/',
        'redisDir'     => APP_PATH . '/redis/',
        'genericDir'     => APP_PATH . '/generic/'
    ],
    'adminResources' => [
        "user"    => [],
        "post"    => ["create", "publish", "getByUser", "getByCarMake","getByExtra"],
        "carMake" => ["get"],
        "notFound" => ["index"],
        "index" => ["index"]
    ],
    'userResources' => [
        "user"    => [],
        "post"    => ["create", "getByUser", "getByCarMake","getByExtra"],
        "carMake" => ["get"],
        "notFound" => ["index"],
        "index" => ["index"]
    ],
    'guestResources' => [
        "user"    => ["login", "registration"],
        "post"    => ["getByUser", "getByCarMake","getByExtra"],
        "carMake" => [],
        "notFound" => ["index"],
        "index" => ["index"]
    ] 
]);