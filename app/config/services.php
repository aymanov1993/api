<?php
use Phalcon\Db\Adapter\Pdo\Postgresql as DbAdapter;
use Phalcon\Mvc\View;
use Vokuro\Acl\Acl;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Mvc\Model\Manager as ModelsManager;

$di->setShared('config', function () {
    return include APP_PATH . '/config/config.php'; 
});

$di->set('db', function () {
    $config = $this->getConfig();
    return new DbAdapter([
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname,
        "port"     => $config->database->port
    ]);
});

$di->set('router', function () {
    return require APP_PATH . '/config/routes.php';
});

$di->setShared('AclResources', function() {
    $pr = [];
    if (is_readable(APP_PATH . '/config/privateResources.php')) {
        $pr = include APP_PATH . '/config/privateResources.php';
    }
    return $pr;
});

$di->set('acl', function () {
    $acl = new Acl();
    $pr = $this->getShared('AclResources')->privateResources->toArray();
    $acl->addPrivateResources($pr);
    return $acl;
});

$di->set('view', function() {
    return new View();
}, true);

$di->set("dispatcher", function () {
    // Create an events manager
    $eventsManager = new EventsManager();

    // Listen for events produced in the dispatcher using the Security plugin
    $eventsManager->attach(
        "dispatch:beforeExecuteRoute",
        new SecurityPlugin()
    );

    // Handle exceptions and not-found exceptions using NotFoundPlugin
    $eventsManager->attach(
        "dispatch:beforeException",
        new NotFoundPlugin()
    );

    $dispatcher = new Dispatcher();

    // Assign the events manager to the dispatcher
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
}, true);

$di->set('redis', function () {
    $config = $this->getConfig();
    $redis = new Redis($config->redis->host, $config->redis->port);
    $redis->connect();
    $redis->auth($config->redis->password);
    
    return $redis;
});

$di->set(
    "modelsManager",
    function() {
        return new ModelsManager();
    }
);

$di->set(
    "urls",
    function() {
        return [
            'baseUrl' => 'http://' . $_SERVER['SERVER_NAME'],
            'requestUrl' => 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'],
            'errorsUrl' => 'http://' . $_SERVER['SERVER_NAME'] . '/api/errors',
            'apiUrl' => 'http://' . $_SERVER['SERVER_NAME'] . '/api',
            'imageUrl' => 'C:/xampp/htdocs/api/public/images/'
        ];
    }
);

$di->set(
    "api",
    function() use ($di) {
        return array_values(
                        [
                            [
                                "registration" => 
                                [
                                    "link" => $di["urls"]['apiUrl']. "/users",
                                    "method" => "POST",
                                    "param" =>
                                    [
                                        "name",
                                        "email",
                                        "password"
                                    ]
                                ],
                                "login" => 
                                [
                                    "link" => $di["urls"]['apiUrl']. "/login",
                                    "method" => "POST",
                                    "param" =>
                                    [
                                        "email",
                                        "password"
                                    ]
                                ],
                                "createPost" => 
                                [
                                    "link" => $di["urls"]['apiUrl']. "/posts",
                                    "method" => "POST",
                                    "param" =>
                                    [
                                        "title",
                                        "description",
                                        "price",
                                        "is_manual",
                                        "token",
                                        "picture",
                                        "car_make",
                                        "car_color"
                                    ]
                                ],
                                "publishPost" => 
                                [
                                    "link" => $di["urls"]['apiUrl']. "/publishPosts/{post_id}",
                                    "method" => "POST",
                                    "param" => ["token"]
                                ],
                                "getPostsByUser" => 
                                [
                                    "link" => $di["urls"]['apiUrl']. "/users/{user_id}/posts",
                                    "method" => "GET",
                                    "param" => ["page"]
                                ],
                                "getPostsByCarMake" => 
                                [
                                    "link" => $di["urls"]['apiUrl']. "/carsMake/{car_make_id}/posts",
                                    "method" => "GET",
                                    "param" => ["page"]
                                ],
                                "getPostsByExtra" => 
                                [
                                    "link" => $di["urls"]['apiUrl']. "/posts",
                                    "method" => "GET",
                                    "param" => ["price","car_color","is_manual", "page"]
                                ],
                                "getCarsMake" => 
                                [
                                    "link" => $di["urls"]['apiUrl']. "/carsMake",
                                    "method" => "GET",
                                    "param" => ["token"]
                                ]
                            ]
                        ]
        );
    }
);