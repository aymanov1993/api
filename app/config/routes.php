<?php

$router = new Phalcon\Mvc\Router(false);

$router->addPost('/users', [
    'controller' => 'user',
    'action' => 'registration'
]);

$router->addPost('/login', [
    'controller' => 'user',
    'action' => 'login'
]);

$router->addPost('/posts', [
    'controller' => 'post',
    'action' => 'create'
]);

$router->addGet('/carsMake', [
    'controller' => 'carMake',
    'action' => 'get'
]);

$router->addPost('/publishPosts/(\d{1,})', [
    'controller' => 'post',
    'action' => 'publish',
    'post_id' => 1
]);

$router->addGet('/users/(\d{1,})/posts', [
    'controller' => 'post',
    'action' => 'getByUser',
    'user_id' => 1
]);

$router->addGet('/carsMake/(\d{1,})/posts', [
    'controller' => 'post',
    'action' => 'getByCarMake',
    'cars_make_id' => 1
]);

$router->addGet('/posts', [
    'controller' => 'post',
    'action' => 'getByExtra'
]);

$router->add('/', [
    'controller' => 'index',
    'action' => 'index'
]);

$router->notFound(
    [
        "controller" => "notFound",
        "action"     => "index",
    ]
);

return $router;