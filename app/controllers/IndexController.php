<?php

use Phalcon\Mvc\Controller;

class IndexController extends Controller
{    
    public function indexAction()
    {
        Generic::returnJsonResponse(200, "Ok",
            [
                "self" => $this->urls['requestUrl'],
                "api" => $this->api
            ]
        );
    }
}