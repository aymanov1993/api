<?php

use Phalcon\Mvc\Controller;
use Phalcon\Crypt;

define('ENCRYPTION_KEY', "!@#$%^&*()_+");
define('ENCRYPTION_METHOD', "aes128");
define('PASS_COST', "11");

class UserController extends Controller
{    
    public function registrationAction()
    {
        $messages = Generic::validateForm(
            [
                "name",
                "email",
                "password"
            ],
            [
                "name" => "/^.{8,}$/",
                "email" => '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(gmail|yahoo).com$/',
                "password" => "/^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,11}$/"
            ],
            [
                "name" => "name is required and should be 8 chars or more.",
                "email" => "email should be valid email address and has @gmail or @yahoo domain.",
                "password" => "password should be between 8 and 12 chars, at least one special character, one capital letter and one number."
            ],
            $_POST
        );
        
        if (count($messages)) {
            $errors_arr = Generic::errorsMessagesArr($messages);
            Generic::returnJsonResponse(400, "Invalid param",
                [
                    "errors" => array_values($errors_arr)
                ]
            );
            return;
        }

        $name = $this->request->getPost("name");
        $email = $this->request->getPost("email");
        $password = $this->request->getPost("password");
        
        $enc_email = openssl_encrypt($email, ENCRYPTION_METHOD, ENCRYPTION_KEY);
        $enc_password = password_hash($password, PASSWORD_BCRYPT, ['cost' => PASS_COST]);
        
        $error = $this->addUser($name, $enc_email, $enc_password);
        
        if ($error <= 0) {
            $error_message = "The email is already exist";
            $error_id = Generic::errorLog($error_message, $this->urls['requestUrl']);
            
            Generic::returnJsonResponse(400, "Invalid param",
                [
                    "errors" => array_values(
                        [
                            [
                                "errorLink" => $this->urls['errorsUrl']. "/{$error_id}",
                                "errorMessage" => $error_message
                            ]
                        ]
                    )
                ]
            );
        } else {
            $key = $this->createSession($error, "user");
            Generic::returnJsonResponse(201, "Created",
                [
                    "self" => $this->urls['requestUrl'],
                    "login" => $this->urls['requestUrl'] . '/login',
                    "token" => $key
                ]
            );           
        }
    }
    
    public function loginAction()
    {
        /*var_dump($this->redis->hget("dee39f2c89da844b08e1", "user_id"));
        var_dump($this->redis->hget("dee39f2c89da844b08e1", "user_role"));
        return;*/
        $messages = Generic::validateForm(
            [
                "email",
                "password"
            ],
            [
                "email" => '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(gmail|yahoo).com$/',
                "password" => "/^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,11}$/"
            ],
            [
                "email" => "email should be valid email address and has @gmail or @yahoo domain.",
                "password" => "password should be between 8 and 12 chars, at least one special character, one capital letter and one number."
            ],
            $_POST
        );
        
        if (count($messages)) {
            $errors_arr = Generic::errorsMessagesArr($messages);
            Generic::returnJsonResponse(400, "Invalid param",
                [
                    "errors" => array_values($errors_arr)
                ]
            );
            return;
        }

        $email = $this->request->getPost("email");
        $password = $this->request->getPost("password");
        
        $enc_email = openssl_encrypt ($email, ENCRYPTION_METHOD, ENCRYPTION_KEY);
        
        $users = Users::query()
                            ->join("UserRole","UserRole.user_id = Users.user_id","UserRole")
                            ->join("Roles","Roles.role_id = UserRole.role_id","Roles")
                            ->andWhere("user_email = :user_email:")
                            ->bind(["user_email" => $enc_email]);
        $users->columns(
                    [
                        "Users.*",
                        "UserRole.*",
                        "Roles.*",
                    ]
                );        
        $users = $users->execute();
        
        $user = $users[0] ?? (new Users());
        
        if (password_verify($password, $user->users->user_password)) {
            $key = $this->createSession($user->users->user_id, $user->roles->role_name);
            Generic::returnJsonResponse(200, "Ok",
                [
                    "self" => $this->urls['requestUrl'],
                    "token" => $key
                ]
            );
            return;
        }
        
        $error_message = "Invalid email or password";
        $error_id = Generic::errorLog($error_message, $this->urls['requestUrl']);
        
        Generic::returnJsonResponse(401, "Unauthorized",
            [
                "errors" => array_values(
                    [
                        [
                            "errorLink" => $this->urls['errorsUrl']. "/{$error_id}",
                            "errorMessage" => $error_message
                        ]
                    ]
                )
            ]
        );
    }
    
    private function createSession(int $user_id, string $user_role) : string
    {
        $key = bin2hex(random_bytes(10));
        $this->redis->hset($key, "user_id", $user_id);
        $this->redis->hset($key, "user_role", $user_role);
        $this->redis->expire($key, 28800);
        
        return $key;
    }
    
    private function addUser(string $name, string $enc_email, string $enc_password) : int
    {
        $user = new Users();
        
        $user->user_name = $name;
        $user->user_email = $enc_email;
        $user->user_password = $enc_password;
        
        try {
            if ($user->save() === false) {
                /*$messages = $user_role->getMessages();
                foreach ($messages as $message) {
                    echo $message, "\n";
                }*/
                return -1;
            }
        } catch (Exception $e) {
            return -1;
        }
                
        $user_role = new UserRole();
        
        $user_role->role_id = 1;
        $user_role->user_id = $user->user_id;
        
        if ($user_role->save() === false) {
            return -2;
        }
        
        return $user->user_id;
    }
}