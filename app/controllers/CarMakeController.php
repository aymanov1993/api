<?php

use Phalcon\Mvc\Controller;

class CarMakeController extends Controller
{
    public function getAction()
    {
        $key = "CarsMake";
        //$this->redis->delete($key);
        $result = $this->redis->get($key);
        
        if (empty($result)) {
            $cars_make = CarsMake::find()->toArray();
            $result = serialize($cars_make);
            $this->redis->set($key, $result);
        } else {
            $cars_make = unserialize($result);
        }

        Generic::returnJsonResponse(200, "Ok",
            [
                "self" => $this->urls['requestUrl'],
                "carsMake" => array_values($cars_make)
            ]
        );
    }
}