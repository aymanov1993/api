<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class PostController extends Controller
{
    public function createAction()
    {        
        $messages = Generic::validateForm(
            [
                "title",
                "description",
                "car_make"
            ],
            [
                "title" => "/^.{20,100}$/",
                "description" => "/^.{100,}$/",
                "car_make" => "/^\d{1,}$/"
            ],
            [
                "title" => "The post title is required and between 20 and 100 characters.",
                "description" => "The post description is required there is no max limit but the minimum is 100 characters.",
                "car_make" => "Invalid car make id."
            ],
            $_POST
        );
        
        if (count($messages)) {
            $errors_arr = Generic::errorsMessagesArr($messages);
            Generic::returnJsonResponse(400, "Invalid param",
                [
                    "errors" => array_values($errors_arr)
                ]
            );
            return;
        }

        $title = $this->request->getPost("title");
        $description = $this->request->getPost("description");
        $car_make = $this->request->getPost("car_make");
        $token = $this->request->getPost("token");
        $user_id = $this->redis->hget($token, "user_id");
        $car_color = $this->request->getPost("car_color");
        $price = $this->request->getPost("price");
        $is_manual = $this->request->getPost("is_manual");
        $image_name = "";
        
        if ($this->request->hasFiles() == true) {
            $baseLocation = $this->urls['imageUrl'];

            // Print the real file names and sizes
            foreach ($this->request->getUploadedFiles() as $file) {
                if(!empty($file->getName())){
                    $image_name = $file->getName();
                }
                //Move the file into the application
                $file->moveTo($baseLocation . $file->getName());
            }
        }
        
        $post = new Posts();
        
        $post->post_title = $title;
        $post->post_description = $description;
        $post->post_cars_make_id = $car_make;
        $post->post_picture = $image_name;
        $post->post_extra = json_encode(["car_color" => $car_color, "price" => $price, "is_manual" => $is_manual]);
        $post->post_is_published = 'f';
        $post->post_user_id = $user_id;
        
        try {
            if ($post->save() === false) {
                $messages = $post->getMessages();
                $errors_arr = Generic::errorsMessagesArr($messages);
                
                Generic::returnJsonResponse(400, "Invalid param",
                    [
                        "errors" => array_values($errors_arr)
                    ]
                );
                return;
            }
        } catch (Exception $e) {
            $error_message = $e->getMessage();
            $error_id = Generic::errorLog($error_message, $this->urls['requestUrl']);
            
            Generic::returnJsonResponse(400, "Invalid param",
                [
                    "errors" => array_values(
                        [
                            [
                                "errorLink" => $this->urls['errorsUrl']. "/{$error_id}",
                                "errorMessage" => $error_message
                            ]
                        ]
                    )
                ]
            );
            return;
        }

        Generic::returnJsonResponse(201, "Created",
            [
                "self" => $this->urls['requestUrl'],
                "post" => $this->urls['requestUrl'] . "/{$post->post_id}"
            ]
        );
    }
    
    public function publishAction()
    {
        $post_id = $this->dispatcher->getParam('post_id');

        $response = new Response();
        try {
            Posts::updatePost(
                $this,
                [
                    "post_is_published" => "t"
                ],
                ["post_id" => [$post_id]]
            );
        } catch (Exception $e) {
            $error_message = $e->getMessage();
            $error_id = Generic::errorLog($error_message, $this->urls['requestUrl']);
            
            Generic::returnJsonResponse(400, "Invalid param",
                [
                    "errors" => array_values(
                        [
                            [
                                "errorLink" => $this->urls['errorsUrl']. "/{$error_id}",
                                "errorMessage" => $error_message
                            ]
                        ]
                    )
                ]
            );
            return;
        }
        
        Generic::returnJsonResponse(200, "Ok",
            [
                "self" => $this->urls['requestUrl'],
                "post" => $this->urls['apiUrl'] . "/posts/{$post_id}"
            ]
        );
    }
    
    public function getByUserAction()
    {
        $user_id = $this->dispatcher->getParam('user_id');
        $page = $this->request->getQuery("page");
        $page = (empty($page))?1:$page;
        $page_size = 10;
        
        $columns = [
            "post_id AS post",
            "post_cars_make_id AS cars_make",
            "post_extra AS extra",
            "post_picture AS picture_url",
            "post_user_id AS publisher"
        ];
        
        $posts = Posts::query()
                    ->columns($columns)
                    ->where('post_user_id = :post_user_id:')
                    ->andWhere("post_is_published = 't'")
                    ->bind(['post_user_id' => $user_id])
                    ->limit($page_size, ($page - 1) * $page_size)
                    ->execute()
                    ->toArray();

        foreach ($posts as $key => $post) {
            $post["post"] = $this->urls['apiUrl'] ."/posts/{$post["post"]}";
            $post["cars_make"] = $this->urls['apiUrl'] . "/carsMake/{$post["cars_make"]}";
            $post["extra"] = json_decode($post["extra"]);
            $post["picture_url"] = (!empty($post["picture_url"]))?$this->urls['apiUrl'] . "/images/{$post["picture_url"]}":"";
            $post["publisher"] = $this->urls['apiUrl'] . "/users/{$post["publisher"]}";
            $posts[$key] = $post;
        }
        
        Generic::returnJsonResponse(200, "Ok",
            [
                "self" => $this->urls['requestUrl'],
                "posts" => array_values($posts)
            ]
        );
    }
    
    public function getByCarMakeAction()
    {
        $cars_make_id = $this->dispatcher->getParam('cars_make_id');
        $page = $this->request->getQuery("page");
        $page = (empty($page))?1:$page;
        $page_size = 10;
        
        $columns = [
            "post_id AS post",
            "post_cars_make_id AS cars_make",
            "post_extra AS extra",
            "post_picture AS picture_url",
            "post_user_id AS publisher"
        ];
        
        $posts = Posts::query()
                    ->columns($columns)
                    ->where('post_cars_make_id = :post_cars_make_id:')
                    ->andWhere("post_is_published = 't'")
                    ->bind(['post_cars_make_id' => $cars_make_id])
                    ->limit($page_size, ($page - 1) * $page_size)
                    ->execute()
                    ->toArray();
        
        $headers = $this->request->getHeaders();
        
        foreach ($posts as $key => $post) {
            if ($headers["Accept"] != "application/xml") {
                $post["post"] = $this->urls['apiUrl'] . "/posts/{$post["post"]}";
                $post["cars_make"] = $this->urls['apiUrl'] . "/carsMake/{$post["cars_make"]}";
                $post["publisher"] = $this->urls['apiUrl'] . "/users/{$post["publisher"]}";
            }
            $post["picture_url"] = (!empty($post["picture_url"]))?$this->urls['apiUrl'] . "/images/{$post["picture_url"]}":"";
            $post["extra"] = json_decode($post["extra"], true);
            $posts[$key] = $post;
        }
        
        if ($headers["Accept"] != "application/xml") {
            Generic::returnJsonResponse(200, "Ok",
                [
                    "self" => $this->urls['requestUrl'],
                    "posts" => array_values($posts)
                ]
            );
        } else {
            Generic::returnXmlResponse(200, "Ok", $posts);
        }   
    }
    
    public function getByExtraAction()
    {
        $price = $this->request->getQuery("price");
        $car_color = $this->request->getQuery("car_color");
        $is_manual = $this->request->getQuery("is_manual");
        $page = $this->request->getQuery("page");
        $page = (empty($page))?1:$page;
        $page_size = 10;
        
        $criteria_arr = [];
        if (!empty($price)) {
            $criteria_arr["price"] = [$price];
        }
        
        if (!empty($car_color)) {
            $criteria_arr["car_color"] = [$car_color];
        }
        
        if (!empty($is_manual)) {
            $criteria_arr["is_manual"] = [$is_manual];
        }
        
        if (empty($criteria_arr)) {
            $error_message = "You have to send at least one search criteria";
            $error_id = Generic::errorLog($error_message, $this->urls['requestUrl']);
            
            Generic::returnJsonResponse(400, "Invalid param",
                [
                    "errors" => array_values(
                        [
                            [
                                "errorLink" => $this->urls['errorsUrl']. "/{$error_id}",
                                "errorMessage" => $error_message
                            ]
                        ]
                    )
                ]
            );
            return;
        }
        
        $columns = [
            "post_id AS post",
            "post_cars_make_id AS cars_make",
            "post_extra AS extra",
            "post_picture AS picture_url",
            "post_user_id AS publisher"
        ];
        
        $posts = Posts::whereExtra($this, $criteria_arr, $columns, ["post_is_published" => ["'t'"]], $page, $page_size);
        
        foreach ($posts as $key => $post) {
            $post["post"] = $this->urls['apiUrl'] . "/posts/{$post["post"]}";
            $post["cars_make"] = $this->urls['apiUrl'] . "/carsMake/{$post["cars_make"]}";
            $post["picture_url"] = (!empty($post["picture_url"]))?$this->urls['apiUrl'] . "/images/{$post["picture_url"]}":"";
            $post["publisher"] = $this->urls['apiUrl'] . "/users/{$post["publisher"]}";
            $post["extra"] = json_decode($post["extra"], true);
            
            $posts[$key] = $post;
        }

        Generic::returnJsonResponse(200, "Ok",
            [
                "self" => $this->urls['requestUrl'], 
                "posts" => array_values($posts)
            ]
        );
    }
}