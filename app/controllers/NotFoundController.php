<?php

use Phalcon\Mvc\Controller;

class NotFoundController extends Controller
{    
    public function indexAction()
    {
        Generic::returnJsonResponse(404, "Page not found",
            [
                "self" => $this->urls['requestUrl'],
                "api" => $this->api
            ]
        );
    }
}