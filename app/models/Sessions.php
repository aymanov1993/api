<?php

use Phalcon\Mvc\Model;

class Sessions extends Model
{
	public $session_id;
    public $session_user_id;
    public $session_token;
    public $session_time;
    public $session_last_action;
    
    public function initialize()
    {
        $this->belongsTo("session_user_id", "Users", "user_id");
    }
    
    public function getSource()
    {
        return "sessions";
    }
}
