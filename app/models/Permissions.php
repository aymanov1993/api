<?php

use Phalcon\Mvc\Model;

class Permissions extends Model
{
	public $permission_id;
    public $permission_desc;
    
    public function initialize()
    {
        $this->belongsTo("permission_id", "Role_permission", "permission_id");
    }
    
    public function getSource()
    {
        return "permissions";
    }
}
