<?php

use Phalcon\Mvc\Model;

class Users extends Model
{
	public $user_id;
    public $user_name;
    public $user_email;
    public $user_password;
    
    public function initialize()
    {
        $this->belongsTo("user_id", "Posts", "post_user_id");
    }
    
    public function getSource()
    {
        return "users";
    }
}
