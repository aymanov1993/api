<?php

use Phalcon\Mvc\Model;

class UserRole extends Model
{
	public $user_id;
    public $role_id;
    
    public function initialize()
    {
        $this->belongsTo("user_id", "Users", "user_id");
        $this->belongsTo("role_id", "Roles", "role_id");
    }
    
    public function getSource()
    {
        return "user_role";
    }
}
