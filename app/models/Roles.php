<?php

use Phalcon\Mvc\Model;

class Roles extends Model
{
	public $role_id;
    public $role_name;
    
    public function initialize()
    {
        $this->belongsTo("role_id", "Role_permission", "role_id");
        $this->belongsTo("role_id", "User_role", "role_id");
    }
    
    public function getSource()
    {
        return "roles";
    }
}
