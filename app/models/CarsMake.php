<?php

use Phalcon\Mvc\Model;

class CarsMake extends Model
{
	public $cars_make_id;
	public $cars_make_name;
    public $cars_make_country;
    
    public function initialize()
    {
        $this->belongsTo("cars_make_id", "Posts", "post_cars_make_id");
    }
    
    public function getSource()
    {
        return "cars_make";
    }
}
