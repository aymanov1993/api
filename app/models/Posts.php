<?php

use Phalcon\Mvc\Model;
use Phalcon\Db;

class Posts extends Model
{
	public $post_id;
    public $post_title;
    public $post_description;
    public $post_cars_make_id;
    public $post_picture;
    public $post_extra;
    public $post_is_published;
    public $post_user_id;
    
    public function initialize()
    {
        $this->belongsTo("post_cars_make_id", "CarsMake", "cars_make_id");
        $this->belongsTo("post_user_id", "Users", "user_id");
        //$this->useDynamicUpdate(true);
        //$this->skipAttributes(["post_is_published", "post_id"]);
    }
    
    public function getSource()
    {
        return "posts";
    }
    
    public function onConstruct() {
        /*Model::setup(array(    
            'notNullValidations' => false
        ));*/
    }
    
    public static function updatePost(PostController $controller,$set,$where)
    {
        $set_stmt = "";
        foreach ($set as $col => $value){
            $set_stmt .= " posts.{$col} = :{$col}:, ";
        }
        $where_stmt = "";
        foreach ($where as $col => $value){
            $where_stmt .= " posts.{$col} IN (". implode(",", $value) .") AND ";
        }
        
        $set_stmt = substr ( $set_stmt, 0, -2);
        $where_stmt = substr ( $where_stmt, 0, -5);

        $dml = "UPDATE posts SET {$set_stmt} WHERE {$where_stmt}";
        $controller->modelsManager->executeQuery($dml, $set);
    }
    
    public static function whereExtra(PostController $controller, $where_extra, $columns, $where, $page, $page_size)
    {
        $where_stmt = "";
        $join_stmt = "";
        $select_stmt = implode(",", $columns);
        
        foreach ($where_extra as $col => $value){
            $join_stmt .= " join jsonb_each_text(posts.post_extra) extra{$col} ON (extra{$col}.key = '{$col}' AND extra{$col}.value IN ('" . implode("','", $value) . "'))  ";
            $where_stmt .= " (extra{$col}.key = '{$col}' AND extra{$col}.value IN ('". implode("','", $value) ."')) AND ";
        }
        
        foreach ($where as $col => $value){
            $where_stmt .= " posts.{$col} IN (". implode(",", $value) .") AND ";
        }
        
        $where_stmt = substr ( $where_stmt, 0, -5);
        $join_stmt = substr ( $join_stmt, 0, -2);
        
        $dml = "SELECT {$select_stmt} FROM posts {$join_stmt} WHERE {$where_stmt} LIMIT $page_size OFFSET (($page - 1) * $page_size)";
        return $controller->db->fetchAll($dml, Db::FETCH_ASSOC);
    }
}
