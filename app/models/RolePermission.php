<?php

use Phalcon\Mvc\Model;

class RolePermission extends Model
{
	public $role_id;
    public $permission_id;
    
    public function initialize()
    {
        $this->belongsTo("role_id", "Roles", "role_id");
        $this->belongsTo("permission_id", "Permissions", "permission_id");
    }
    
    public function getSource()
    {
        return "role_permission";
    }
}
