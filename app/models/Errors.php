<?php

use Phalcon\Mvc\Model;

class Errors extends Model
{
	public $error_id;
	public $error_message;
    public $error_link;
    public $error_time;
    
    public function initialize()
    {
        //$this->belongsTo("", "", "");
    }
    
    public function getSource()
    {
        return "errors";
    }
}
