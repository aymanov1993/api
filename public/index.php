<?php
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Application;

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

try {
    $di = new FactoryDefault();
    
    include APP_PATH . "/config/services.php";
    
    $config = $di->getConfig();
    
    include APP_PATH . '/config/loader.php';
    
    $application = new Application($di);
    
    echo $application->handle()
        ->getContent();
} catch (Exception $e) {
    $error_message = $e->getMessage();
    $error_id = Generic::errorLog($error_message, $di['urls']['requestUrl']);
    
    Generic::returnJsonResponse(500, "Internal server error",
        [
            "errors" => array_values(
                [
                    [
                        "errorLink" => $di['urls']['errorsUrl']. "/{$error_id}",
                        "errorMessage" => $error_message
                    ]
                ]
            )
        ]
    );
	//echo nl2br(htmlentities($e->getTraceAsString()));
}